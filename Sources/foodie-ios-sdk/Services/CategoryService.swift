//
//  CategoryService.swift
//  FoodieKit
//
//  Created by Nik Zakirin on 21/06/2017.
//  Copyright © 2017 Digital Foodie Ltd. All rights reserved.
//

/**
 *  CategoryService is responsible for retrieving the product category hierarchy
 *
 */
public class CategoryService {
    public static let shared = CategoryService()
    
    public func fetchTopLevelCategories(completion: @escaping (_ productCategories: [ProductCategory]?) -> ()) {
        /* Configure session, choose between:
         * defaultSessionConfiguration
         * ephemeralSessionConfiguration
         * backgroundSessionConfigurationWithIdentifier:
         And set session-wide properties, such as: HTTPAdditionalHeaders,
         HTTPCookieAcceptPolicy, requestCachePolicy or timeoutIntervalForRequest.
         */
        let sessionConfig = URLSessionConfiguration.default
        
        /* Create session, and optionally set a URLSessionDelegate. */
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        
        /* Create the Request:
         entry/category_children (GET https://api.foodie.fi:443/api/entry/category_children)
         */
        
        guard var URL = URL(string: Config.kBaseURL+"/api/entry/category_children") else { return }
        let URLParams = [
            "trId": UUID().uuidString,
            ]
        URL = URL.appendingQueryParameters(URLParams)
        var request = URLRequest(url: URL)
        request.httpMethod = "GET"
        
        /* Start a new Task */
        let task = session.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            DispatchQueue.main.async {
                if (error == nil) {
                    // Success
                    if let data = data, let productCategoryBox: ProductCategoryBox = try? unbox(data: data) {
                        completion(productCategoryBox.categories)
                    } else {
                        completion(nil)
                    }
                }
                else {
                    // Failure
                    print("URL Session Task Failed: %@", error!.localizedDescription);
                    completion(nil)
                }
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }
}
