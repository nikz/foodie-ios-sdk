//
//  FoodieKit.swift
//  FoodieKit
//
//  Created by Nik Zakirin on 21/06/2017.
//  Copyright © 2017 Digital Foodie Ltd. All rights reserved.
//

public class FoodieKit {
    public class func setApplicationConfigurations(
        appId: String,
        appSecret: String,
        baseURL: String) {
        
        Config.kAppId = appId
        Config.kAppSecret = appSecret
        Config.kBaseURL = baseURL
    }
}
