//
//  Config.swift
//  FoodieKit
//
//  Created by Nik Zakirin on 21/06/2017.
//  Copyright © 2017 Digital Foodie Ltd. All rights reserved.
//

enum Config {
    static var kAppId = ""
    static var kAppSecret = ""
    static var kBaseURL = ""
}
