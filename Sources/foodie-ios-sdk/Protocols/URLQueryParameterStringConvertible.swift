//
//  URLQueryParameterStringConvertible.swift
//  FoodieKit
//
//  Created by Nik Zakirin on 21/06/2017.
//  Copyright © 2017 Digital Foodie Ltd. All rights reserved.
//

protocol URLQueryParameterStringConvertible {
    var queryParameters: String { get }
}
