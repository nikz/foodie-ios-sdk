//
//  foodie-ios-sdk.h
//  foodie-ios-sdk
//
//  Created by Nik Zakirin on 21/06/2017.
//  Copyright © 2017 Digital Foodie Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for foodie-ios-sdk.
FOUNDATION_EXPORT double foodie_ios_sdkVersionNumber;

//! Project version string for foodie-ios-sdk.
FOUNDATION_EXPORT const unsigned char foodie_ios_sdkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <foodie_ios_sdk/PublicHeader.h>


