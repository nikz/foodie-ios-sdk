//
//  ProductCategory.swift
//  FoodieKit
//
//  Created by Nik Zakirin on 21/06/2017.
//  Copyright © 2017 Digital Foodie Ltd. All rights reserved.
//

import Foundation

/**
 *  Struct to represent a product category
 *
 */
public struct ProductCategory: Unboxable {

    /// Product category name
    public let name: String
    
    /// Product category identifier
    public let identifier: Int
    
    
    /// Identifier of the parent product category
    public let parentIdentifier: Int
    
    /// EAN code used for the display of the product image
    public let exampleEAN: Int
    
    /// - returns: URL for the product image for the example EAN
    public var imageURL: URL? {
        let urlString = "https://d2i1ofz69vk5mz.cloudfront.net/images/entries/180x220/\(self.exampleEAN)_0.png"
        return URL(string: urlString)
    }
    
    init(unboxer: Unboxer) throws {
        self.name = try unboxer.unbox(key: "name")
        self.identifier = try unboxer.unbox(key: "id")
        self.parentIdentifier = try unboxer.unbox(key: "parent_id")
        self.exampleEAN = try unboxer.unbox(key: "example_ean")
    }
}

struct ProductCategoryBox: Unboxable {
    let categories: [ProductCategory]
    
    init(unboxer: Unboxer) throws {
        self.categories = try unboxer.unbox(keyPath: "message.children")
    }
}
