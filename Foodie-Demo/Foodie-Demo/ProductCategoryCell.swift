//
//  ProductCategoryCell.swift
//  Foodie-Demo
//
//  Created by Nik Zakirin on 21/06/2017.
//  Copyright © 2017 Digital Foodie Ltd. All rights reserved.
//

import UIKit
import FoodieKit
import Kingfisher

class ProductCategoryCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var categoryNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.masksToBounds = true
        layer.cornerRadius = 8
    }
    
    var productCategory: ProductCategory? {
        didSet {
            updateUI()
        }
    }
}

fileprivate extension ProductCategoryCell {
    func updateUI() {
        guard let category = productCategory else { return }
        
        if let imageURL = category.imageURL {
            categoryImageView.kf.setImage(with: imageURL)
        } else {
            categoryImageView.image = nil
        }
        categoryNameLabel.text = category.name.capitalized
    }
}
