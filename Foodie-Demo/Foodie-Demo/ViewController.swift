//
//  ViewController.swift
//  Foodie-Demo
//
//  Created by Nik Zakirin on 21/06/2017.
//  Copyright © 2017 Digital Foodie Ltd. All rights reserved.
//

import UIKit
import FoodieKit

private enum Const {
    static let kProductCategoryCellIdentifier = "ProductCategoryCell"
}

class ViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!
    
    fileprivate var categories: [ProductCategory]?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        CategoryService.shared.fetchTopLevelCategories { [weak self] productCategories in
            guard let `self` = self else { return }
            self.categories = productCategories
            self.collectionView?.reloadData()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let columnCount = 2
        let interItemSpacing = flowLayout.minimumInteritemSpacing * CGFloat(columnCount-1)
        let width = (collectionView.bounds.size.width - flowLayout.sectionInset.left - flowLayout.sectionInset.right - interItemSpacing) / CGFloat(columnCount)
        let ratio = CGFloat(1.2)
        
        flowLayout.itemSize = CGSize(width: width, height: width*ratio)
        flowLayout.invalidateLayout()
    }
}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.categories != nil ? self.categories!.count : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ProductCategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: Const.kProductCategoryCellIdentifier, for: indexPath) as! ProductCategoryCell
        
        cell.productCategory = self.categories?[indexPath.item]
        
        return cell
    }
}

