# Foodie Mobile iOS SDK #

Foodie iOS SDK makes it simple for you to showcase and sell your products in your iOS app.

## Integrate the FoodieKit framework
- - - -

We recommend integrating our SDK using CocoaPods or Carthage, which simplifies version updates and dependency management. In the meantime, follow these installation steps to add FoodieKit as a dynamic framework.

#### Step 1: Clone the repository

```
git clone git@bitbucket.org:nikz/foodie-ios-sdk.git
```

#### Step 2: Add the SDK to your app
**File** -> **Add files to "YOUR_PROJECT"** -> Select **FoodieKit.xcodeproj** from the file chooser. This will add **FoodieKit.xcodeproj** as a sub-project.

#### Step 3: Link the framework to the app's target
Drag **FoodieKit.framework** from the **Products** folder to your app's *Embedded Binaries* section.

Now your app knows about the FoodieKit framework.

#### Step 4: Import the dependency
```swift
import FoodieKit
```

## Set the API key and secret
- - - -
Initialize FoodieKit with your application key, secret, and base URL
```swift
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    FoodieKit.setApplicationConfigurations(appId: "APPLICATION_ID", appSecret: "APPLICATION_SECRET", baseURL: "BASE_URL")
    return true
}
```

## Try the FoodieKit Demo
- - - -
**Foodie-Demo** folder contains a sample project which utilises FoodieKit for retrieving product categories and displaying the data in a collection view.

Have any questions? Reach out to [support@digitalfoodie.com](mailto:support@digitalfoodie.com).

